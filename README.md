CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module provides a new webform element called "JS Step" that provides a
way to divide webforms in to steps that do not require any page loads or Ajax
calls.

* Project page:
   https://www.drupal.org/project/webform_js_steps

* To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/webform_js_steps


REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://drupal.org/project/webform)
 
RECOMMENDATIONS
---------------

As standard back-end validation of webform will only trigger on the final
submit, client-side validation is recommended.

  * Clientside Validation (https://www.drupal.org/project/clientside_validation)


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information


CONFIGURATION
-------------

When enabled this module provides a new type of element, 'JS Step', to be added
to a webform. These step elements will allow you to group other elements in 
steps, that will be shown one by one.
As soon as at least 2 steps are added, navigation will be added based on step
configuration.


MAINTAINERS
-----------

Current maintainers:
 * Laurens Van Damme (L_VanDamme) - https://www.drupal.org/u/l_vandamme
 * Alex Kuzava (nginex) - https://www.drupal.org/u/nginex
