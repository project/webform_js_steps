<?php

namespace Drupal\webform_js_steps\Element;

use Drupal\Core\Render\Element\Container;

/**
 * Provides a 'JS Step' element.
 *
 * @FormElement("js_step")
 */
class JsStep extends Container {}
