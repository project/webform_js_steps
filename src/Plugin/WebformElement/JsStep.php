<?php

namespace Drupal\webform_js_steps\Plugin\WebformElement;

use Drupal\webform\Plugin\WebformElement\ContainerBase;

/**
 * Provides a 'js_step' element.
 *
 * @WebformElement(
 *   id = "js_step",
 *   label = @Translation("JS Step"),
 *   description = @Translation("Provides an element for creating form steps that do not require page loads."),
 *   category = @Translation("Containers"),
 * )
 */
class JsStep extends ContainerBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return parent::defineDefaultProperties();
  }

}
